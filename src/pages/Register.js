import {Container, Card, Col, Row, Button, Form} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import Swal from 'sweetalert2'
import { useHistory } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register(){

const { user } = useContext(UserContext);
const [email, setEmail] = useState('');
const [password1, setPassword1] = useState('');
const [password2, setPassword2] = useState('');
const [isActive, setIsActive] = useState(false);
const [firstName, setFirstName] = useState('');
const [lastName, setLastName] = useState('');
let history = useHistory();

function registerUser(e){
  e.preventDefault();
  fetch('http://localhost:4000/users/checkEmail',{
    method: 'POST',
    headers:{
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        email:email
    })
  })
  .then(res => res.json())
  .then(data => {

    if(data === false){
      fetch('http://localhost:4000/users/register',{
        method: 'POST',
        headers:{
            'Content-Type':'application/json'
        },
        body: JSON.stringify({
            firstName:firstName,
            lastName:lastName,
            email:email,
            password:password1,
        })
      })
      .then(res => res.json())
      .then(data => {
        console.log(`${data}dataDATA`);
        Swal.fire({
            title:"Registration Successful",
            icon:"success",
            text:"Welcome to Zuitt"
        });
        history.push('/login')
      }) 

    }
    else{
      Swal.fire({
          title:"Duplicate email found",
          icon:"error",
          text:"Please provide different email"
      })
    }
  })

  setEmail('');
  setPassword2('');
  setPassword1('');
  setFirstName('');
  setLastName('');

}

useEffect(() => {
  if((firstName !== '' &&lastName !== '' && password1 !== '' && password2 !== '') && (password1 === password2) )
  {
    setIsActive(true);
  }
  else{
    setIsActive(false);
  }
}, [firstName, lastName, email, password1, password2])

    return(
        (user.id !== null) ?
            history.push('/')
        :
        <Container fluid="lg">
        <Row lg={2}>
        <Card>
        <Form onSubmit={(e) => registerUser(e)}>
            <Col>
            <Form.Label><h2>Register</h2></Form.Label>
            </Col>
            <Col>
                  <Form.Group className="mb-1" controlId="firstName" xs={12} md={6} lg={4}>
                    <Form.Label>First Name</Form.Label>
                    <Form.Control
                    column lg="2" 
                    type="text" 
                    placeholder="Enter First Name" value={firstName}
                    onChange = {e => setFirstName(e.target.value)}
                    required
                     />
                  </Form.Group>
            </Col>
            <Col>
                  <Form.Group className="mb-1" controlId="lastName" xs={12} md={6} lg={4}>
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control
                    column lg="2" 
                    type="text" 
                    placeholder="Enter Last Name" value={lastName}
                    onChange = {e => setLastName(e.target.value)}
                    required />
                  </Form.Group>
            </Col>
            <Col>
                  <Form.Group className="mb-1" controlId="userEmail" xs={12} md={6} lg={4}>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                    column lg="2" 
                    type="email" 
                    placeholder="Enter Email" value={email}
                    onChange = {e => setEmail(e.target.value)}
                    required />
                    <Form.Text className="text-muted">
                      We'll never share your email with anyone else.
                    </Form.Text>
                  </Form.Group>
            </Col>
            <Col>
                  <Form.Group className="mb-1" controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                    column lg="2"
                    type="password" 
                    placeholder="Password" 
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    required />
                  </Form.Group>
            </Col>
            <Col>
                    <Form.Group className="mb-3" controlId="password2" xs={12} md={6} lg={4}>
                      <Form.Label>Confirm Password</Form.Label>
                      <Form.Control
                      column lg="2"
                      type="password" 
                      placeholder="Password" 
                      value={password2}
                      onChange={e => setPassword2(e.target.value)}
                    required />
                    </Form.Group>
            </Col>
            <Col>
                  { isActive ?
                          <Button variant="success" type="submit" id="submitBtn">
                            Submit
                          </Button>
                          :
                          <Button variant="dark" type="submit" id="submitBtn" disabled>
                            Submit
                          </Button>
                        }
            </Col>
        </Form>
        </Card>
        </Row>
        </Container>
    )
}
