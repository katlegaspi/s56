import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){
	const data={
		title: "Badangga's Beverages",
		content: "It's the quenchiest!",
		destination: "/products",
		label: "Shop Now!"
	}
	return(
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	);
}